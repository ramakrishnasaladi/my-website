import { Component, OnInit } from "@angular/core";

import { LogService } from "../../services/log.service";

import Logger from "../../models/logger";

@Component({
  selector: "app-loggers",
  templateUrl: "./loggers.component.html",
  styleUrls: ["./loggers.component.scss"],
})
export class LoggersComponent implements OnInit {
  logs: Logger[];
  selectedLog: Logger;
  loaded: boolean = false;

  constructor(private logService: LogService) {}

  ngOnInit(): void {
    this.logService.stateClear.subscribe((clear) => {
      if (clear) {
        this.selectedLog = {
          id: "",
          title: "",
          description: "",
          created: null,
        };
      }
    });
    this.logService.getLogs().subscribe((logs) => {
      this.logs = logs;
      this.loaded = true;
    });
  }

  onSelect(log: Logger) {
    this.selectedLog = log;
    this.logService.setFormLog(log);
  }

  deleteLog(log: Logger): void {
    this.logService.deleteLog(log);
  }
}
