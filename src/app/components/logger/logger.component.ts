import { Component, OnInit } from "@angular/core";

import Logger from "../../models/logger";
import { LogService } from "../../services/log.service";

@Component({
  selector: "app-logger",
  templateUrl: "./logger.component.html",
  styleUrls: ["./logger.component.scss"],
})
export class LoggerComponent implements OnInit {
  id = "1254";
  title = null;
  description = null;
  created = null;
  isNew: boolean = true;

  constructor(private logService: LogService) {}

  ngOnInit(): void {
    this.logService.selectedLog.subscribe((log) => {
      if (log.id) {
        this.isNew = false;
        this.id = log.id;
        this.title = log.title;
        this.created = log.created;
        this.description = log.description;
      }
    });
  }

  clearState() {
    this.isNew = true;
    this.id = "";
    this.title = "";
    this.description = "";
    this.created = null;
    this.logService.clearState();
  }

  onSubmit(): void {
    if (this.isNew) {
      const newLog: Logger = {
        id: this.generateId(),
        title: this.title,
        description: this.description,
        created: new Date(),
      };
      this.logService.addLog(newLog);
    } else {
      // log to be updated
      const updLog = {
        id: this.id,
        title: this.title,
        description: this.description,
        created: new Date(),
      };
      this.logService.updateLog(updLog);
    }
    this.clearState();
  }

  generateId(): string {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (
      c
    ) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }
}
