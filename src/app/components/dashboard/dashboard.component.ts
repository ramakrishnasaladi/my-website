import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  // props
  skills = [];

  constructor() {}

  ngOnInit(): void {
    this.skills = [
      {
        id: 1,
        title: 'Angular',
        features: [
          {
            description: 'Experience',
            value: '4 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 2,
        title: 'React',
        features: [
          {
            description: 'Experience',
            value: '2 years',
          },
          {
            description: 'Last Used',
            value: 2020,
          },
        ],
      },
      {
        id: 3,
        title: 'NodeJs',
        features: [
          {
            description: 'Experience',
            value: '1 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 4,
        title: 'AngularJs',
        features: [
          {
            description: 'Experience',
            value: '3 years',
          },
          {
            description: 'Last Used',
            value: 2017,
          },
        ],
      },
      {
        id: 5,
        title: 'HTML5',
        features: [
          {
            description: 'Experience',
            value: '10 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 6,
        title: 'C#',
        features: [
          {
            description: 'Experience',
            value: '8 years',
          },
          {
            description: 'Last Used',
            value: 2020,
          },
        ],
      },
      {
        id: 7,
        title: 'ASP.net MVC',
        features: [
          {
            description: 'Experience',
            value: '3 years',
          },
          {
            description: 'Last Used',
            value: 2020,
          },
        ],
      },
      {
        id: 8,
        title: 'SCSS',
        features: [
          {
            description: 'Experience',
            value: '2 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 9,
        title: 'Web API',
        features: [
          {
            description: 'Experience',
            value: '1 years',
          },
          {
            description: 'Last Used',
            value: 2018,
          },
        ],
      },
      {
        id: 10,
        title: 'JQuery',
        features: [
          {
            description: 'Experience',
            value: '3 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 11,
        title: 'Karma & Jasmine',
        features: [
          {
            description: 'Experience',
            value: '4 years',
          },
          {
            description: 'Last Used',
            value: 2023,
          },
        ],
      },
      {
        id: 12,
        title: 'SQL',
        features: [
          {
            description: 'Experience',
            value: '3 years',
          },
          {
            description: 'Last Used',
            value: 2017,
          },
        ],
      },
      {
        id: 13,
        title: 'Nunit',
        features: [
          {
            description: 'Experience',
            value: '1 years',
          },
          {
            description: 'Last Used',
            value: 2018,
          },
        ],
      },
    ];
  }
}
