import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";

import Logger from "../models/logger";

@Injectable({
  providedIn: "root",
})
export class LogService {
  logs: Logger[];
  private logSource = new BehaviorSubject<Logger>({
    id: null,
    title: null,
    description: null,
  });
  selectedLog = this.logSource.asObservable();

  private stateSource = new BehaviorSubject<boolean>(true);
  stateClear = this.stateSource.asObservable();

  constructor() {
    this.logs = [];
  }

  getLogs(): Observable<Logger[]> {
    if (localStorage.getItem("logs") === null) {
      this.logs = [
        {
          id: "1",
          title: "Add",
          description: `Add title and description to a particular task and click on 'Add Task'. The task will be stored in browser memory `,
          created: new Date(),
        },
        {
          id: "2",
          title: "Edit",
          description: `You can edit any log just by clicking on the title`,
          created: new Date(),
        },
        {
          id: "3",
          title: "Delete",
          description: `You click on the 'close' icon to delete a particular log`,
          created: new Date(),
        },
      ];
    } else {
      this.logs = JSON.parse(localStorage.getItem("logs"));
    }
    return of(this.logs);
  }

  setFormLog(log: Logger) {
    this.logSource.next(log);
  }

  addLog(log: Logger) {
    this.logs.unshift(log);

    // add to local storage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  updateLog(log: Logger) {
    this.logs.forEach((item, index) => {
      if (item.id === log.id) {
        this.logs.splice(index, 1);
      }
    });
    this.logs.unshift(log);
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  deleteLog(log: Logger) {
    this.logs.forEach((current, index) => {
      if (current.id === log.id) {
        this.logs.splice(index, 1);
      }
    });
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  clearState() {
    this.stateSource.next(true);
  }
}
