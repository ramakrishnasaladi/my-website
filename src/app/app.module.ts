import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./components/header/header.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { FooterComponent } from "./components/footer/footer.component";
import { InterestsComponent } from "./components/interests/interests.component";
import { LoggersComponent } from "./components/loggers/loggers.component";
import { LoggerComponent } from "./components/logger/logger.component";
import { NotfoundComponent } from "./components/notfound/notfound.component";

import { LogService } from "./services/log.service";
import { CarouselComponent } from './components/carousel/carousel.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    ProfileComponent,
    FooterComponent,
    InterestsComponent,
    LoggersComponent,
    LoggerComponent,
    NotfoundComponent,
    CarouselComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [LogService],
  bootstrap: [AppComponent],
})
export class AppModule {}
