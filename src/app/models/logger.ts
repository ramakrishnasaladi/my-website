export default class Logger{
    id: string;
    title: string;
    description?: string;
    created?: Date
}